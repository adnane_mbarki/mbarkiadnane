package ma.ac.emi.qcm.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ma.ac.emi.qcm.entities.Matiere;
import ma.ac.emi.qcm.entities.Niveau;

@Repository
public interface MatiereRepository extends JpaRepository<Matiere,Long>{
	@Query("select m from Matiere m where m.nom like:x")
	Page<Matiere> getPageMatiereByNom(@Param("x")String mc,Pageable pageable);
	
    Matiere getMatiereById(long id);
	 @Query("select n from Matiere n JOIN n.niveau as f where f.id =:x")
	    public List<Matiere> findByMatiere(@Param("x") Long id);
	 @Query("select n from Niveau n where n.nom =:y")
	    public List<Niveau> findbyNom(@Param("y") String nom);
	  @Query("select m from  Matiere m group by nom")
	    List<Matiere> GroupByNom();
	  @Query("select m from  Matiere m where m.nom =:y ")
	    List<Matiere> getMatieresById(@Param("y") String nom);
}
