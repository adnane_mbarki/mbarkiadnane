package ma.ac.emi.qcm.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ma.ac.emi.qcm.entities.Classe;

public interface ClasseRepository extends JpaRepository<Classe,Long> {
    Classe getClasseById(Long id);
    @Query("select c from Classe c where c.nom like :x or c.niveau.nom like :x or c.niveau.formation.nom like :x ")
    public Page<Classe> chercher(@Param("x")String mc,Pageable pageable);
}
