package ma.ac.emi.qcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ma.ac.emi.qcm.entities.Formateur;


public interface FormateurRepository extends JpaRepository<Formateur, String>{
    Formateur getFormateurByLogin(String login);
   
  
}
