package ma.ac.emi.qcm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import ma.ac.emi.qcm.entities.*;
import ma.ac.emi.qcm.repository.*;

@SpringBootApplication
public class QcmApplication {

	@Autowired
	ThemeRepository themeRepo;
	@Autowired
	FormationRepository formationRepo;
	@Autowired
	NiveauRepository niveauRepo;
	@Autowired
	MatiereRepository matiereRepo;
	@Autowired
	FormateurRepository formateurRepository;
	@Autowired
	QCMRepository qcmRepository;
	@Autowired
	ClasseRepository classeRepo;
	@Autowired
	QuestionRepository questionRepo;

	@Autowired
	ReponseRepository reponseRepository;

	public static void main(String[] args) {
		SpringApplication.run(QcmApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(EleveRepository er) {
		return args -> {
			Formation formation = new Formation("ingénieurs");
			formationRepo.save(formation);
			Formation formation1 = new Formation("Doctorale");
			formationRepo.save(formation1);
			
			formationRepo.save(formation1);
			Formation formation2 = new Formation("Master");
			formationRepo.save(formation2);
			Niveau niveau0 = new Niveau("1ere annee", formation1);
			Niveau niveau4 = new Niveau("2eme annee", formation2);
			niveauRepo.save(niveau4);
			niveauRepo.save(niveau0);
			Niveau niveau1 = new Niveau("1ere annee", formation);
			niveauRepo.save(niveau1);
			Niveau niveau2 = new Niveau("2eme annee", formation);
			niveauRepo.save(niveau2);
			Niveau niveau3 = new Niveau("3eme annee", formation);
			niveauRepo.save(niveau3);
			Formateur formateur3 = new Formateur("Euldj", "Euldj@gmail.com", "Euldj", "123456");
			Formateur formateur = new Formateur("Kabbaj", "Kabbaj@gmail.com", "Kabbaj", "1234");
			Matiere matiere = new Matiere("JEE", niveau2);
			Matiere matiere1 = new Matiere("Design Pattern", niveau2);
			Matiere matiere2 = new Matiere("Architecture D'ordinateur", niveau1);
			matiereRepo.save(matiere);
			matiereRepo.save(matiere1);
			matiereRepo.save(matiere2);
			formateur3.getMatieres().add(matiere2);
			formateur.getMatieres().add(matiere);
			formateurRepository.save(formateur3);
			formateurRepository.save(formateur);
			Formateur formateur1 = new Formateur("Anwar", "Anwar@gmail.com", "Anwar", "12345");
			formateur1.getMatieres().add(matiere1);
			formateurRepository.save(formateur1);
           Classe c=new Classe("G Info",niveau2);
           Classe c1=new Classe("G Mis",niveau1);
           classeRepo.save(c1);
           classeRepo.save(c);
           Eleve eleve=new Eleve("Adnane1998","mbarki.adnane98@gmail.com","Adnane","12345",5225);
           Eleve eleve1=new Eleve("Ali1998","ali_m'hamm@gmail.com","Ali","123456",56543);
           er.save(eleve);
           er.save(eleve1);
		
		};
	}

}
