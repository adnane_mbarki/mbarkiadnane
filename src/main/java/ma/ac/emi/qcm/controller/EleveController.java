package ma.ac.emi.qcm.controller;

import ma.ac.emi.qcm.entities.Classe;
import ma.ac.emi.qcm.entities.Eleve;
import ma.ac.emi.qcm.entities.Formateur;
import ma.ac.emi.qcm.repository.ClasseRepository;
import ma.ac.emi.qcm.repository.EleveRepository;
import ma.ac.emi.qcm.repository.FormateurRepository;
import ma.ac.emi.qcm.repository.FormationRepository;
import ma.ac.emi.qcm.repository.MatiereRepository;
import ma.ac.emi.qcm.repository.NiveauRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/eleves")
public class EleveController {
	@Autowired
	ClasseRepository cr;
	@Autowired
	EleveRepository er;
	@Autowired
	FormateurRepository fr;
	@Autowired
	FormationRepository formationrep;
	@Autowired
	NiveauRepository niveaurep;
	@Autowired
	MatiereRepository mr;
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("Eleve", new Eleve());
		model.addAttribute("Eleves", er.findAll());
		return "eleves/Eleves";
	}

	@RequestMapping(value = "/addEleve", method = RequestMethod.POST)
	public String addEleve( Model model,Eleve e) {
		er.save(e);
		return "redirect:/eleves/";

	}
	@GetMapping(path = "/addEleve")
	public String addEleve(Model model) {
		model.addAttribute("Eleve", new Eleve());
		return "eleves/FormEleve";
	}
	@PostMapping(path = "/saveEleve")
	public String enregistrerE(Model model, Eleve e,Long  id) {
		er.save(e);
	
		Classe c = cr.findById(id).get();
		
		for (Formateur e1 : c.getFormateurs()) {
			if (e1.getLogin() == e.getLogin()) {
				model.addAttribute("Exception", "Ce formateur est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getEleves().add(e);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + id;
	}
	@GetMapping(path = "/supprimerEleve")
	public String supprimerE(Model model, String login, Long id) {
		Classe c = cr.findById(id).get();
		c.getEleves().remove(er.findById(login).get());
		cr.save(c);
		return "redirect:/eleves/detailsClasse?id=" + id;
	}
	@GetMapping(path = "/supprimer")
	public String supprimerE(Model model, String login) {
		er.deleteById(login);
		
		return "redirect:/classes/";
	}

	@GetMapping(path = "/editerEleve")
	public String editerE(Model model, String login) {
		Eleve e = er.findById(login).get();
		model.addAttribute("Eleve", e);
		return "eleves/FormEleve";
	}
	@GetMapping(path = "/attribuerClasseE")
	public String attribuerE(Model model, String login) {
		Eleve e = er.findById(login).get();
		model.addAttribute("Eleve", e);
		model.addAttribute("Classes", cr.findAll());
		return "eleves/attribuerClasse";
	}
	@GetMapping(path = "/attribuerClasse")
	public String attribuerClasse(Model model, String login, Long cl) {
		Eleve e = er.findById(login).get();
		Classe c = cr.findById(cl).get();
		for (Eleve e1 : c.getEleves()) {
			if (e1.getLogin() == e.getLogin()) {
				model.addAttribute("Exception", "Cet élève est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getEleves().add(e);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + cl;
	}
	@PostMapping(path = "/attribuerClasse1")
	public String attribuerClasse1(Model model, String login, @RequestParam("c_id") Long cl) {
		Eleve e = er.findById(login).get();
		Classe c = cr.findById(cl).get();
		for (Eleve e1 : c.getEleves()) {
			if (e1.getLogin() == e.getLogin()) {
				model.addAttribute("Exception", "Cet élève est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getEleves().add(e);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + cl;
	}
	

}
