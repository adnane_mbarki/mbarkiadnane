package ma.ac.emi.qcm.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ma.ac.emi.qcm.entities.Classe;
import ma.ac.emi.qcm.entities.Formateur;
import ma.ac.emi.qcm.entities.Matiere;
import ma.ac.emi.qcm.repository.ClasseRepository;
import ma.ac.emi.qcm.repository.EleveRepository;
import ma.ac.emi.qcm.repository.FormateurRepository;
import ma.ac.emi.qcm.repository.FormationRepository;
import ma.ac.emi.qcm.repository.MatiereRepository;
import ma.ac.emi.qcm.repository.NiveauRepository;
@Controller
@RequestMapping("/formateurs")
public class FormateurControlleur {
	@GetMapping("/")
	public String index(Model model) {
	
		
		model.addAttribute("Formateurs", fr.findAll());
		
		return "formateurs/Formateur";
	}
	@Autowired
	ClasseRepository cr;
	@Autowired
	EleveRepository er;
	@Autowired
	FormateurRepository fr;
	@Autowired
	FormationRepository formationrep;
	@Autowired
	NiveauRepository niveaurep;
	@Autowired
	MatiereRepository mr;
	@GetMapping(path = "/ajouterFormateur")
	public String addFormateur(Model model) {
		model.addAttribute("Formateur", new Formateur());
		Collection<Matiere> listcatégorie_Matier=mr.GroupByNom();
		
		
		model.addAttribute("listcat_matiere",listcatégorie_Matier);
		return "formateurs/FormFormateur";
	}
	@GetMapping(path = "/ajouterFormateur1")
	public String addFormateur1(Model model) {
		model.addAttribute("Formateur", new Formateur());
		Collection<Matiere> listcatégorie_Matier=mr.GroupByNom();
		
		
		model.addAttribute("listcat_matiere",listcatégorie_Matier);
		return "formateurs/FormFormateur2";
	}


	@PostMapping(path = "/saveFormateur")
	public String enregistrerF(Model model, Formateur f,Long  id ,@RequestParam("m_id") Long matiere_id){
		
		f.getMatieres().add(mr.getMatiereById(matiere_id));
		fr.save(f);
		Classe c = cr.findById(id).get();
		
		for (Formateur f1 : c.getFormateurs()) {
			if (f1.getLogin() == f.getLogin()) {
				model.addAttribute("Exception", "Ce formateur est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getFormateurs().add(f);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + id;
		
	}


	@PostMapping(path = "/saveFormateur1")
	public String enregistrerF(Model model, Formateur f,@RequestParam("m_id") Long matiere_id){
		
		String nom=mr.getMatiereById(matiere_id).getNom();
		Collection <Matiere> matieres=(mr.getMatieresById(nom));
		f.getMatieres().addAll(matieres);
		fr.save(f);
	
	
		return "redirect:/formateurs/";
		
	}



	@GetMapping(path = "/supprimerFormateur")
	public String supprimerF(Model model, String login, Long id) {
		Classe c = cr.findById(id).get();
		c.getFormateurs().remove(fr.findById(login).get());
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + id;
	}

	@GetMapping(path = "/editerFormateur")
	public String editerF(Model model, String login) {
		Formateur f = fr.findById(login).get();
		model.addAttribute("Formateur", f);
		return "formateurs/FormFormateur";
	}
	@GetMapping(path = "/supprimer")
	public String supprimer(Model model, String login) {
		fr.deleteById(login);
		
		return "redirect:/classes/";
	}
	@GetMapping(path = "/attribuerClasseF")
	public String attribuerF(Model model, String login) {
		Formateur f = fr.findById(login).get();
		model.addAttribute("Formateur", f);
		model.addAttribute("Classes", cr.findAll());
		return "classes/AttribuerClasse";
	}

}
