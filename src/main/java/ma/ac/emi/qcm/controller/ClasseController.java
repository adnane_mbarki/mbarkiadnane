package ma.ac.emi.qcm.controller;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import ma.ac.emi.qcm.entities.Classe;
import ma.ac.emi.qcm.entities.Eleve;
import ma.ac.emi.qcm.entities.Formateur;
import ma.ac.emi.qcm.entities.Formation;
import ma.ac.emi.qcm.entities.Matiere;
import ma.ac.emi.qcm.entities.Niveau;
import ma.ac.emi.qcm.repository.ClasseRepository;
import ma.ac.emi.qcm.repository.EleveRepository;
import ma.ac.emi.qcm.repository.FormateurRepository;
import ma.ac.emi.qcm.repository.FormationRepository;
import ma.ac.emi.qcm.repository.MatiereRepository;
import ma.ac.emi.qcm.repository.NiveauRepository;

@Controller
@RequestMapping("/classes")
public class ClasseController {
	@Autowired
	ClasseRepository cr;
	@Autowired
	EleveRepository er;
	@Autowired
	FormateurRepository fr;
	@Autowired
	FormationRepository formationrep;
	@Autowired
	NiveauRepository niveaurep;
	@Autowired
	MatiereRepository mr;

	@GetMapping("/")
	public String index(Model model,@RequestParam(name="page",defaultValue="0")int p,@RequestParam(name="size",defaultValue="2")int s,@RequestParam(name="mc",defaultValue="")String mc) {
		model.addAttribute("Classes", cr.findAll());
		model.addAttribute("Eleves", er.findAll());
		model.addAttribute("Formateurs", fr.findAll());
		model.addAttribute("Niveau", niveaurep.findAll());
		Page<Classe> pageclasses=cr.chercher("%"+mc+"%",PageRequest.of(p,s));
		model.addAttribute("listclasses",pageclasses.getContent());
		int[] pages=new int[pageclasses.getTotalPages()];
		model.addAttribute("pages",pages);
		model.addAttribute("size",s);
		model.addAttribute("pageCourante",p);
		return "classes/Home";
	}
	@GetMapping("/index")
	public String index1(Model model,@RequestParam(name="page",defaultValue="0")int p,@RequestParam(name="size",defaultValue="2")int s,@RequestParam(name="mc",defaultValue="")String mc) {
		model.addAttribute("Classes", cr.findAll());
		model.addAttribute("Eleves", er.findAll());
		model.addAttribute("Formateurs", fr.findAll());
		model.addAttribute("Niveau", niveaurep.findAll());
		Page<Classe> pageclasses=cr.chercher("%"+mc+"%",PageRequest.of(p,s));
		model.addAttribute("listclasses",pageclasses.getContent());
		int[] pages=new int[pageclasses.getTotalPages()];
		model.addAttribute("pages",pages);
		model.addAttribute("size",s);
		model.addAttribute("pageCourante",p);
		return "classes/Home";
	}

	@RequestMapping(value="/ajouterClasse",method=RequestMethod.GET)
	public String addClasse(Model model) {
		model.addAttribute("Classe", new Classe());
		model.addAttribute("Formation", new Formation());
		Collection<Formation> listcatégorie_formation=formationrep.findAll();
		model.addAttribute("listcat_formation",listcatégorie_formation);
		model.addAttribute("Niveau", new Niveau());
		Collection<Niveau> listcatégorie_niveau=niveaurep.GroupByNom();
		model.addAttribute("listcat_niveau",listcatégorie_niveau);
		return "classes/FormClasse2";
	}
	

	@PostMapping(path = "/saveClasse")
	public String enregistrer(Model model, Classe c,Niveau n,Formation f) {
		formationrep.save(f);
		
		n.setFormation(f);
		niveaurep.save(n);
		c.setNiveau(n);
		cr.save(c);
	
		
		return "redirect:/classes/";
	}

	@PostMapping(path = "/save")
	public String suivant(Model model,@RequestParam("n_id") Long id1,Integer f_id, String classe_nom,Long classe_id) {
	     
	  Classe c=new Classe();
	  c.setNom(classe_nom);
	  c.setId(classe_id);
	  c.setNiveau(niveaurep.findById(id1).get());
	  cr.save(c);
	  return "redirect:/classes/";
	}
	@PostMapping(path = "/suivant")
	public String suivant(Model model,@RequestParam("f_id") Long id,@RequestParam("nom") String classe_nom,@RequestParam("id") Long classe_id) {
	    
		 Classe c=new Classe();
		  c.setNom(classe_nom);
		  c.setId(classe_id);
	     model.addAttribute("f_id", id);
	     model.addAttribute("classe_id",classe_id);
	     model.addAttribute("classe_nom",classe_nom);
		
		Collection<Niveau> listcatégorie_niveau=niveaurep.findByFormatio(id);
		model.addAttribute("Niveau", listcatégorie_niveau);
		return "classes/FormClasse3";
	}

	@GetMapping(path = "/supprimerClasse")
	public String supprimer(Model model, Long id) {
		cr.deleteById(id);
		return "redirect:/classes/";
	}

	@GetMapping(path = "/editerClasse")
	public String editer(Model model, Long id) {
		Classe c = cr.findById(id).get();
		model.addAttribute("Classe", c);
		return "classes/FormClasse";
	}

	@GetMapping(path = "/detailsClasse")
	public String details(Model model, Long id) {
		Classe c = cr.findById(id).get();
		model.addAttribute("Classe", c);
		return "classes/DetailsClasse";
	}

	/*----------------------------------------------------------------------------*/

	@GetMapping(path = "/ajouterEleve")
	public String addEleve(Model model,Long id) {
		Collection<Eleve> eleves=er.findAll();
		model.addAttribute("Eleves",eleves);
		  model.addAttribute("classe_id",id);
		Classe c=cr.findById(id).get();
		return "eleves/elevedispo";
	}

	@PostMapping(path = "/saveEleve")
	public String enregistrerE(Model model, Eleve e) {
		er.save(e);
		return "redirect:/classes/";
	}
	@GetMapping(path = "/supprimer")
	public String supprimerE(Model model, String login) {
		er.deleteById(login);
		
		return "redirect:/classes/";
	}
	@GetMapping(path = "/supprimerEleve")
	public String supprimerE(Model model, String login,Long id) {
		Classe c = cr.findById(id).get();
		c.getEleves().remove(er.findById(login).get());
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + id;
	}

	@GetMapping(path = "/editerEleve")
	public String editerE(Model model, String login) {
		Eleve e = er.findById(login).get();
		model.addAttribute("Eleve", e);
		return "eleves/FormEleve";
	}

	/*----------------------------------------------------------------------------*/

	@GetMapping(path = "/ajouterFormateur")
	public String ajouterFormateur(Model model,Long id) {
		model.addAttribute("Formateur", new Formateur());
		
		Classe c=cr.findById(id).get();
		Long id1=c.getNiveau().getId();
	   model.addAttribute("classe_id",id);
	   Collection<Matiere> listcatégorie_Matier=mr.findByMatiere(id1);
		
		model.addAttribute("listcat_matiere",listcatégorie_Matier);
		return "formateurs/FormFormateur1";
	}
	@GetMapping(path = "/saveMatiere")
	public String saveMatiere(Model model,@RequestParam("m_id") Long matiere_id,@RequestParam("c_id") Long classe_id) {
	
		Matiere m=mr.getMatiereById(matiere_id);
	     
		
		model.addAttribute("m_nom",m);
		 model.addAttribute("classe_id",classe_id);
		
		 Collection<Formateur>list_fromateurs=m.getFormateurs();
		model.addAttribute("Formateurs",list_fromateurs);
	 

		return "formateurs/Formateurdispo";
	}
	@PostMapping(path = "/add")
	public String add(Model model,@RequestParam("c_id") Long classe_id ,String login){
		
	
	
		Formateur f = fr.findById(login).get();
		Classe c = cr.findById(classe_id).get();
		
		for (Formateur f1 : c.getFormateurs()) {
			if (f1.getLogin() == f.getLogin()) {
				model.addAttribute("Exception", "Ce formateur est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getFormateurs().add(f);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + classe_id;
		
	}
	@PostMapping(path = "/saveFormateur")
	public String enregistrerF(Model model, Formateur f,@RequestParam("m_id") Long matiere_id){
	f.getMatieres().add(mr.getMatiereById(matiere_id));
		fr.save(f);
		return "redirect:/classes/";
	}

	@GetMapping(path = "/supprimerFormateur")
	public String supprimerF(Model model, String login, Long id) {
		Classe c = cr.findById(id).get();
		c.getFormateurs().remove(fr.findById(login).get());
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + id;
	}

	@GetMapping(path = "/editerFormateur")
	public String editerF(Model model, String login) {
		Formateur f = fr.findById(login).get();
		model.addAttribute("Formateur", f);
		return "formateurs/FormFormateur";
	}

	/*----------------------------------------------------------------------------*/

	@GetMapping(path = "/attribuerClasseE")
	public String attribuerE(Model model, String login) {
		Eleve e = er.findById(login).get();
		model.addAttribute("Eleve", e);
		model.addAttribute("Classes", cr.findAll());
		return "classes/AttribuerClasse";
	}

	@GetMapping(path = "/attribuerClasseF")
	public String attribuerF(Model model, String login) {
		Formateur f = fr.findById(login).get();
		model.addAttribute("Formateur", f);
		model.addAttribute("Classes", cr.findAll());
		return "classes/AttribuerClasse";
	}

	@GetMapping(path = "/attribuerClasse")
	public String attribuerClasse(Model model, String login, Long cl) {
		Eleve e = er.findById(login).get();
		Classe c = cr.findById(cl).get();
		for (Eleve e1 : c.getEleves()) {
			if (e1.getLogin() == e.getLogin()) {
				model.addAttribute("Exception", "Cet élève est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getEleves().add(e);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + cl;
	}

	@GetMapping(path = "/attribuerClasse1")
	public String attribuerClasse1(Model model, String login, Long cl) {
		Formateur f = fr.findById(login).get();
		Classe c = cr.findById(cl).get();
		for (Formateur f1 : c.getFormateurs()) {
			if (f1.getLogin() == f.getLogin()) {
				model.addAttribute("Exception", "Ce formateur est dèja dans cette classe");
				return "erreur";
			}
		}
		c.getFormateurs().add(f);
		cr.save(c);
		return "redirect:/classes/detailsClasse?id=" + cl;

	}

}
